import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex, axios)

const store = new Vuex.Store({
  state: {
    galleryJsonPhoto: {},
    headerObject: {}
  },
  getters: {
    getObjectGalleryPhoto (state) {
      return state.galleryJsonPhoto
    },
    getHeaderObject (state) {
      return state.headerObject
    }
  },
  mutations: {
    setCallFirstPhoto (state, massPhotoJson) {
      let reg = /web|development|design/
      for (let key in massPhotoJson[0].acf) {
        if (key.match(reg)) {
          console.log(massPhotoJson[0].acf[key])
          // Vue.set(state.galleryJsonPhoto, key.replace('_', ' '), resJson[0].acf[key])
        }
      }
    },
    setGalleryJsonPhoto (state, resJson) {
      let reg = /web|development|design/
      for (let key in resJson[0].acf) {
        if (key.match(reg)) {
          Vue.set(state.galleryJsonPhoto, key.replace('_', ' '), resJson[0].acf[key])
        }
      }
    },
    setHeaderObject (state, header) {
      state.headerObject = header[0].acf
      console.log(header[0].acf)
    }
  },
  actions: {
    getDataJson ({commit}, link = '') {
      const url = link
      const fullUrl = `${url}`
      axios.get(fullUrl)
        .then(response => {
          commit('setGalleryJsonPhoto', response.data)
          commit('setHeaderObject', response.data)
          commit('setCallFirstPhoto', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    }
  }
})

export default store
