src
  assets
  components
      routes
          files1.vue
          files2.vue - <!-- this main pages app -->
          files3.vue
      files1.vue
      files2.vue - <!-- this additional pages app -->
      files3.vue
      ...... .vue
  router
      index.js - <!-- this main routers for app -->
  scss - <!-- this lass and saas app -->
  store
      actions - <!-- this actions from store (vuex) -->
      helpers.js - <!-- this actions from store (vuex) -->
      mutations.js - <!-- this mutations (setters) from store (vuex) -->
      store.js - <!-- this index from store (vuex)-->
  utils
      api.js - <!-- this all functions, all request (get, post) app -->
  App.vue - <!-- this main pages app -->
  Main.js - <!-- this main pages app -->