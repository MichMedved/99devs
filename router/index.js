import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/routes/Home'
import Contacts from '@/components/routes/Feedback'
// import modvuex from '@/components/modvuex'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Homepage', component: Home },
    { path: '/contacts', name: 'Contacts', component: Contacts }
    // { path: '/modvuex', component: modvuex }
  ]
})
